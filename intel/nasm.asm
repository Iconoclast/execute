[section] .data
; nothing to do in this section for a simple execution
; Note that memory reference expressions are enclosed within square brackets.

[section] .text
XOR eax, eax ; smaller, faster, Intel-optimized:  Set EAX to zero.
RET ; Return zero to main().
