.intel_mnemonic
.intel_syntax noprefix
# http://sourceware.org/binutils/docs/as/i386_002dVariations.html
# http://sourceware.org/binutils/docs/as/i386_002dMnemonics.html

# http://sourceware.org/binutils/docs-2.17/as/Ld-Sections.html
.section .bss
# nothing to add to BSS section
.section .data
# nothing required inside the DATA section
.section .text # using the code section for program entry point
.global	_main
_main: #       Line 1 # int main(void) { // entry point into executable
XOR eax, eax # Line 2 #     return (0);  // success code to OS
RET #          Line 3 # }
