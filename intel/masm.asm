.386 ; the oldest base Intel instruction set
.model flat, C ; http://msdn.microsoft.com/en-us/library/ss9fh0d6

; For program segment names:  http://msdn.microsoft.com/en-us/library/d06y3478

.data ; segment name:  _DATA
; nothing to do in this section for a simple execution

.code ; segment name:  _TEXT
_main PROC
XOR eax, eax ; smaller, faster, Intel-optimized:  Set EAX to zero.
RET ; Return zero to main().
_main ENDP

END ; http://msdn.microsoft.com/en-us/library/wxy1fb5k
